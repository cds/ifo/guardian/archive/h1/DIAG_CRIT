from SYS_DIAG import *
import subprocess

#############################################

all_nodes = subprocess.run(
    ['guardctrl', 'list'],
    capture_output=True,
    universal_newlines=True,
    check=True,
).stdout.split()
sei_conf_nodes = [conf for conf in all_nodes if '_SC' in conf or 'BLND' in conf] \
                 + ['SEI_CONF', 'BRSX_STAT', 'BRSY_STAT']

sqz_sub_nodes = [
    'SQZ_SHG',
    'SQZ_OPO_LR',
    'SQZ_CLF_LR',
    'SQZ_LO_LR',
    'SQZ_FC',]

@SYSDIAG.register_test
def SEI_CONFIG_ACTIVE():
    """The SEI config nodes are taken out of the IFO node to allow
    different states suring observation. This check will look at the
    ACTIVE chan where all of the below must be True:

    OP == EXEC
    MODE in [AUTO, MANAGED]
    LOAD_STATUS == DONE
    WORKER != INIT 
    not ERROR
    CONNECT == OK

    (See daemon.py in guardian code for more channel definitions.)
    """
    for node in sei_conf_nodes:
        if node == 'ISI_ETMY_ST1_SC':
            continue
        if not ezca['GRD-{}_ACTIVE'.format(node)]:
            yield '{} not ACTIVE'.format(node)


@SYSDIAG.register_test
def SQZ_SUB_NODES_ACTIVE():
    """The SQZ sub nodes (not manager) are taken out of the IFO node
    to allow us to change to Observing with squeezing or without 
    squeezing with as few changes as possible. This check will look at
    the ACTIVE chan where all of the below must be True:

    OP == EXEC
    MODE in [AUTO, MANAGED]
    LOAD_STATUS == DONE
    WORKER != INIT 
    not ERROR
    CONNECT == OK

    (See daemon.py in guardian code for more channel definitions.)
    """
    for node in sqz_sub_nodes:
        if not ezca['GRD-{}_ACTIVE'.format(node)]:
            yield '{} not ACTIVE'.format(node)
